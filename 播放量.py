TV_movies = [('绿皮书', 8.9),
             ('徒手攀岩', 9.0),
             ('四个春天', 8.9),
             ('玩具总动员4', 8.8),
             ('波西米亚狂想曲', 8.7),
             ('谁先爱上他的', 8.6),
             ('何以为家', 9.0),
             ('五月天人生无限公司', 8.7),
             ('复仇者联盟4：终局之战', 8.6),
             ('无主之作', 8.6)
             ]
b = input("请输入视频名：")
c = float(input("请输入豆瓣评分："))
TV_movies.append((b, c))
e = input("请输入你想删除的视频名")
d = float(input("请输入你想删除的视频的豆瓣评分"))
TV_movies.pop(TV_movies.index((e, d)))

TV_movies = sorted(TV_movies, key=lambda s: s[1], reverse=True)
print("电影豆瓣评分排行榜：")
for item in TV_movies:
    print(item)