T40 = ("T40", "长春-北京", "00:12", "12:20", "12:08")
T298 = ("T298", "长春-北京", "00:06", "10:50", "10:44")
Z158 = ("Z158", "长春-北京", "12:48", "21:06", "08:18")
Z62 = ("Z62", "长春-北京", "21:58", "06:08", "8:20")
list = [T40, T298, Z158, Z62]
print("当前车次信息：")
print("车次\t\t出发站-到达站\t\t出发时间\t到达时间\t历时")
for a, b, c, d, e in list:
    print(a, "\t\t", b, "\t\t", c, "\t\t", d, " \t", e)
i = input("是否加入新车次？（yes or no）")
if i == "no":
    print("欢迎乘车！")
else:
    none = True
    while none:
        train = input("请输入要加入的车次：")
        station = input("请输入出发站和终点站（A-B):")
        setup = input("请输入出发时间：")
        arrivetime = input("请输入到达时间：")
        time = input("请输入历时：")
        list.append((train, station, setup, arrivetime, time))
        m = input("是否继续加入？(yes or no)")
        if m == "no":
            none = False
    print("当前车次信息：")
    print("车次\t\t出发站-到达站\t\t出发时间\t到达时间\t历时")
    for a, b, c, d, e in list:
        print(a, "\t\t", b, "\t\t", c, "\t\t", d, "\t\t", e)