Tank = ['嫦娥', '项羽', '夏侯惇', '亚瑟', '钟无艳']
Warrior = ['典韦', '关羽', '橘右京', '刘备']
Assassin = ['韩信', '兰陵王', '娜可露露']
Mages = ['安琪拉', '不知火舞', '妲己', '上官婉儿']
Marksman = ['伽罗', '公孙离', '鲁班', '孙尚香']
Support = ['大乔', '鬼谷子', '姜子牙']
b = input("请输入一个英雄:")
if b in ["白起", "苏烈", "吕布", "猪八戒"]:
    Tank.append(b)
elif b in ["老夫子", "凯", "盘古", "孙策"]:
    Warrior.append(b)
elif b in ["李白", "露娜", "宫本武藏", "百里玄策"]:
    Assassin.append(b)
elif b in ["小乔", "貂蝉", "奕星"]:
    Mages.append(b)
elif b in ["狄仁杰", "李元芳", "马可波罗", "虞姬"]:
    Marksman.append(b)
elif b in ["孙膑", "蔡文姬", "牛魔", "廉颇"]:
    Support.append(b)
c = input("请输入你想删除的英雄：")
if c in ["嫦娥", "项羽", "夏侯惇", "亚瑟"]:
    Tank.pop(Tank.index(c))
if c in ['典韦', '关羽', '橘右京', '刘备']:
    Warrior.pop(Warrior.index(c))
if c in ['韩信', '兰陵王', '娜可露露']:
    Assassin.pop(Assassin.index(c))
if c in ['安琪拉', '不知火舞', '妲己', '上官婉儿']:
    Mages.pop(Mages.index(c))
if c in ['伽罗', '公孙离', '鲁班', '孙尚香']:
    Marksman.pop(Marksman.index(c))

print('\n====坦克====')
for Tank in Tank:
    print(Tank, end=' ')
print('\n====战士====')
for Warrior in Warrior:
    print(Warrior, end=' ')
print('\n====刺客====')
for Assassin in Assassin:
    print(Assassin, end=' ')
print('\n====法师====')
for Mages in Mages:
    print(Mages, end=' ')
print('\n====射手====')
for Marksman in Marksman:
    print(Marksman, end=' ')
print('\n====辅助====')
for Support in Support:
    print(Support, end=' ')