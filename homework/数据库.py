import pymysql
import os.path
conn = pymysql.connect(host ='127.0.0.1', port = 3306, user = 'yeruixin', password = '123456', database ='subject')
cursor = conn.cursor()
cursor.execute('create table subject (number int(10) primary key, name varchar(20), property varchar) ')
cursor.execute('insert into subject(number, name, property) values (01, "高等数学（二）","必修")')
cursor.execute('insert into subject(number, name, property) values (02, "大学物理", "选修")')
cursor.execute('select * from subject')
print(cursor.fetchall())

cursor.execute('update subject set property = "必修" where number = 02')
cursor.execute('select * from subject')
print(cursor.fetchall())

cursor.execute('delete from subject where number = 02')
cursor.execute('select * from subject')
print(cursor.fetchall())

conn.close()

